package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

public class EmailValidateTest {
	
	//Regular
	@Test
	public void testisValidEmailRegularFormat() {
		boolean email = EmailValidate.isValidEmail("zain_razvi@gmail.com");
		assertTrue("It is a valid email", email);
	}
	
	//Exception
	@Test
	public void testisValidEmailExceptionFormat() {	
		boolean email = EmailValidate.isValidEmail("zainmail");
		assertFalse("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailExceptionHasAtSymbol() {	
		boolean email = EmailValidate.isValidEmail("@zaingmailcom");
		assertFalse("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailExceptionAccountNameIsLowerCase() {	
		boolean email = EmailValidate.isValidEmail("ZK123@gmailcom");
		assertFalse("It is an invalid email", email);
	}
	@Test
	public void testisValidEmailExceptionHasAccountName() {	
		boolean email = EmailValidate.isValidEmail("@gmailcom");
		assertFalse("It is an invalid email", email);
	}
		
	@Test
	public void testisValidEmailExceptionHasDomainName() {	
		boolean email = EmailValidate.isValidEmail("zain");
		assertFalse("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailExceptionHasExtensionName() {	
		boolean email = EmailValidate.isValidEmail("raz@");
		assertFalse("It is an invalid email", email);
	}
	
	//Boundary In
	@Test
	public void testisValidEmailBoundaryInFormat() {	
		boolean email = EmailValidate.isValidEmail("raz@ign.ca");
		assertTrue("It is a valid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryInHasAtSymbol() {	
		boolean email = EmailValidate.isValidEmail("zan@mail.com");
		assertTrue("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryInAccountNameIsLowerCase() {	
		boolean email = EmailValidate.isValidEmail("zk1@gmailcom");
		assertTrue("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryInHasAccountName() {	
		boolean email = EmailValidate.isValidEmail("pam@yahoo.com");
		assertTrue("It is a valid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryInHasDomainName() {	
		boolean email = EmailValidate.isValidEmail("zai@123.com");
		assertTrue("It is an valid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryInHasExtensionName() {	
		boolean email = EmailValidate.isValidEmail("zai@123.ca");
		assertTrue("It is an invalid email", email);
	}
	
	
	//Boundary Out
	@Test
	public void testisValidEmailBoundaryOutFormat() {	
		boolean email = EmailValidate.isValidEmail("zaingmail.com");
		assertFalse("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryOutEmailIsBlank() {	
		boolean email = EmailValidate.isValidEmail("");
		assertFalse("It is an invalid email", email);
	}
	
	@Test
	public void testisValidEmailBoundaryOutHasAtSymbol() {	
		boolean email = EmailValidate.isValidEmail("@zaingmail.com");
		assertFalse("It is an invalid email", email);
	}
}
