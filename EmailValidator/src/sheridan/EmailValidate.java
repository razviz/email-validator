package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidate {
	
	private EmailValidate() {
		
	}
	
	public static boolean isValidEmail(String email) {
		
		String regex = "^[a-z0-9+_.-]+@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
